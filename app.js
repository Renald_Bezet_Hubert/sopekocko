/**
 * require on
 * framework Express
 * packages :
 * DOTENV | MONGOOSE | CORS | BODY-PARSER | PATH | HELMET
 */
require('dotenv').config()
// require framework, packages and models
const express = require('express')
const mongoose = require('mongoose')
const cors = require('cors')
const path = require('path')
const bodyParser = require('body-parser')
const helmet = require('helmet')
const routeSauce = require('./routes/sauce')
const routeUser = require('./routes/user')
// use Express
const app = express()
// DOTENV
console.log(process.env.MONGO_URI)

// Mongoose Connect
mongoose.connect(process.env.MONGO_URI,
  {
    useNewUrlParser: true,
    useUnifiedTopology: true
  })
  .then(() => console.log('Connexion à MongoDB réussie !'))
  .catch(() => console.log('Connexion à MongoDB échouée !'))

// App use..
app.use((req, res, next) => {
  res.setHeader('Access-Control-Allow-Origin', '*')
  res.setHeader('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content, Accept, Content-Type, Authorization')
  res.setHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, PATCH, OPTIONS')
  next()
})

app.use(cors())
// Protect API
app.use(helmet())

// Manage JSON Object
app.use(bodyParser.json())

// image
app.use('/images', express.static(path.join(__dirname, 'images')))

// Routes
app.use('/api/sauces', routeSauce)
app.use('/api/auth', routeUser)

// export for server
module.exports = app
