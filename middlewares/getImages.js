/*
get  Sauce model
require fs to manage file
manage image : delete
 delete use also to change image ( replace old by new one)
 find Sauce id
 modify Image => before check if OldImage === true = delete
*/

const Sauce = require('../models/sauce')
const fs = require('fs')

const deleteImage = (name) => {
  fs.unlink(`images/${name}`, function (e) {
    if (e) {
      throw e
    }
  })
  return true
}

exports.modifyImage = (req, res, next) => {
  Sauce.findOne({ _id: req.params.id })
    .then((sauce) => {
      const name = sauce.imageUrl.split('/images/')[1]
      if (deleteImage(name) === true) {
        next()
      } else {
        res.status(400).json({ error: 'Invalid' })
      }
    })
    .catch((error) => res.status(501).json({ error }))
}
