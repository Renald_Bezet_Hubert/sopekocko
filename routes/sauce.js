/**
 * Use Express
 */
// require to manage & define routes
const express = require('express')
const router = express.Router()

const auth = require('../middlewares/auth')
const multer = require('../middlewares/multer')
const getImages = require('../middlewares/getImages')
const sauceCtrl = require('../controllers/sauce')

// Define routes ( to verify user: place auth before otthers functions)
router.get('/', auth, sauceCtrl.getAllSauces) // all Sauces
router.get('/:id', auth, sauceCtrl.getOneSauce) // One sauce by id
router.post('/', auth, multer, sauceCtrl.createSauce) // create/
router.put('/:id', auth, multer, getImages.modifyImage, sauceCtrl.modifySauce) // modify sauce
router.post('/:id/like', auth, sauceCtrl.likedSauce) // like or diskike
router.delete('/:id', auth, sauceCtrl.deleteSauce) // delete sauce

module.exports = router
