/**
 * require Express, Router method & controller User
 * use router to define post to database( signup/login)
 * export module
 */

const express = require('express')
const router = express.Router()

const userCtrl = require('../controllers/user')

router.post('/signup', userCtrl.signup)
router.post('/login', userCtrl.login)

module.exports = router
