/**
 * Create model USER
 * use Mongoose with .Schema
 * use mongoose-unique-validator => one email for one count
 * Naming packages using with package'name
 */

const mongoose = require('mongoose')
const uniqueValidator = require('mongoose-unique-validator')

const userSchema = mongoose.Schema({
  email: { type: String, required: true, unique: true },
  password: { type: String, reqired: true, unique: true }
})

userSchema.plugin(uniqueValidator)
module.exports = mongoose.model('User', userSchema)
