/**
 * Create model SAUCE
 * use Mongoose with .Schema
 * Naming packages using with package'name
 */

const mongoose = require('mongoose')
// cf DOCUMENTATION we create object SAUCE with type: xxxx, requiredd: boolean
const sauceSchema = mongoose.Schema({
  userId: { type: String, required: true },
  name: { type: String, required: true },
  manufacturer: { type: String, required: true },
  description: { type: String, required: true },
  mainPepper: { type: String, required: true },
  imageUrl: { type: String, required: true },
  heat: { type: Number, required: true },
  // Initialize value '0' to likes & unlikes to start result
  likes: { type: Number, default: 0 },
  dislikes: { type: Number, default: 0 },
  usersLiked: { type: [String] },
  usersDisliked: { type: [String] }
})

module.exports = mongoose.model('Sauce', sauceSchema)
