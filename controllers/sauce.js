/**
 * Get sauce model
 * add node module to manage files ( Sauce's pic) : fs
 * Create, modify, update, delete Sauce:
 * exports.whatwedoSauce = (req, res, next)=> {}
 */
// const sanitize = require("mongo-sanitize")
const Sauce = require('../models/sauce')// Sauce
const fs = require('fs')

// create sauce  (with middleware =req,res,next)
exports.createSauce = (req, res, next) => {
  console.log(req.body)
  const sauceObject = JSON.parse(req.body.sauce)
  delete sauceObject._id
  const sauce = new Sauce({
    // create sauce with elements (Object, like/dislike, userLiked, userDisliked)
    ...sauceObject,
    // likes: 0,
    // dislikes: 0,
    // add folder images to stock sauce's image files
    imageUrl: `${req.protocol}://${req.get('host')}/images/${req.file.filename}`
  })
  console.log(sauce)
  // save created sauce: confirm with message or check error
  sauce.save()
    .then(() => res.status(201).json({ message: 'Votre sauce est enregistrée!' }))
    .catch(error => res.status(400).json({ error }))
}
// Modify Sauce
exports.modifySauce = (req, res, next) => {
  // Verify if have a file
  const sauceObject = req.file
    ? {
        // get info of sauce's form
      // if have a file
        ...JSON.parse(req.body.sauce),
        imageUrl: `${req.protocol}://${req.get('host')}/images/${req.file.filename}`
      } // not
    : { ...req.body }
  // Update the first file matches with correct params( here with id)
  Sauce.updateOne({ _id: req.params.id }, { ...sauceObject, _id: req.params.id })
    .then(() => res.status(201).json({ message: 'Votre sauce est modifiée!' }))
    .catch(error => res.status(400).json({ error }))
}

// After create and possibility to delete
exports.deleteSauce = (req, res, next) => {
  // get the correct sauce with id
  Sauce.findOne({ _id: req.params.id })
    .then(sauce => {
      const filename = sauce.imageUrl.split('/images/')[1]
      // use unlink(fs) to delete image of sauce(id) & delete
      fs.unlink(`images/${filename}`, () => {
        Sauce
          .deleteOne({ _id: req.params.id })
          .then(() => res.status(200).json({ message: 'Votre Sauce a été supprimée!' }))
          .catch(error => res.status(400).json({ error }))
      })
    })
    .catch(error => res.status(500).json({ error }))
}

// GET One Sauce then all Sauces
exports.getOneSauce = (req, res, next) => {
  Sauce.findOne({ _id: req.params.id })
    .then(sauce => res.status(200).json(sauce))
    .catch(error => res.status(400).json({ error }))
}
exports.getAllSauces = (req, res, next) => {
  console.log('getAllSauces')
  Sauce.find()
    .then(sauces => res.status(200).json(sauces))
    .catch(error => res.status(400).json({ error }))
}
// LIKE & DISLIKE Sauce
/**
 * /api/sauces/:id/like
 * .Step 1 = find sauce by id
 * .Step 2 Condition:
 * declare var ( like = req.body.like to manage system of likes)
 * Better to step condition:
 *  -if already like=> error else... code block to push and add to array
 * use updateOne ( $push & $inc)
 * if like === 1 => likes 1 & usersLike
 * else if like=== -1 => dislikes 1 & usersDislike
 * else like ===0 => undo choice
 * likes or dislikes -= 1($inc -1) & $pull to array userId to array
 */
exports.likedSauce = (req, res, next) => {
  Sauce.findOne({ _id: req.params.id })
    .then(sauce => {
      const like = req.body.like
      const user = req.body.userId
      const usersLiked = sauce.usersLiked
      const usersDisliked = sauce.usersDisliked
      const sauceId = req.params.id
      const userId = req.body.userId
      const findSauce = Sauce.findOne({ _id: sauceId })
      // LIKE
      if (like === 1) {
        // Already like => error
        if (usersLiked.includes(userId)) {
          res.status(403).json({ error: 'You have already like' })
        // Like Sauce
        } else {
          findSauce
            .then((sauce) => {
              Sauce.updateOne({ _id: sauceId },
                {
                  $push: { usersLiked: user },
                  $inc: { likes: 1 }
                })
                .then(() => res.status(200).json({ message: 'Like' }))
                .catch((error) => res.status(400).json({ error }))
            })
            .catch((error) => res.status(404).json({ error }))
        }
      // DISLIKE
      } else if (like === -1) {
        // Already Dislike => error
        if (usersDisliked.includes(userId)) {
          res.status(403).json({ error: 'You have already dislike' })
        } else { // Dislike Sauce
          findSauce
            .then((sauce) => {
              Sauce.updateOne({ _id: sauceId },
                {
                  $push: { usersDisliked: user },
                  $inc: { dislikes: 1 }
                })
                .then(() => res.status(200).json({ message: 'Dislike' }))
                .catch((error) => res.status(400).json({ error }))
            })
            .catch((error) => res.status(404).json({ error }))
        }
      } else { // Undo choice like & dislike
        findSauce
          .then((sauce) => {
            // Cancel like
            if (sauce.usersLiked.includes(userId)) {
              Sauce.updateOne({ _id: req.params.id },
                {
                  $inc: { likes: -1 },
                  $pull: { usersLiked: req.body.userId },
                  _id: sauceId
                })
                .then(() => { res.status(201).json({ message: 'Like: Canceled' }) })
                .catch((error) => { res.status(400).json({ error: error }) })
            } else { // Cancel dislike
              Sauce.updateOne({ _id: req.params.id },
                {
                  $inc: { dislikes: -1 },
                  $pull: { usersDisliked: req.body.userId },
                  _id: sauceId
                })
                .then(() => { res.status(201).json({ message: 'Dislike: Canceled' }) })
                .catch((error) => { res.status(400).json({ error: error }) })
            }
          })
          .catch((error) => { res.status(500).json({ error }) })
      }
    })
    .catch(error => res.status(500).json({ error }))
}
