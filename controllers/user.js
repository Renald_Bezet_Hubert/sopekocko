/**
 * Create USER  with signup and login
 *  | BCRYPT & JWT for security
 * MASK email in BDD : replace string[x] into * ( stability function not dependant to package)
 * use exports.whatidoUser
 */

// Get model USER & necessary packages (hash data, token, validate input(string))

const bcrypt = require('bcrypt')
const jsonWebToken = require('jsonwebtoken')
const validator = require('validator')
const User = require('../models/user')

// manage login/signup ( verify count & give Token)
// signup verify correct format for  email then password ( with bcrypt)
// if not ok = message
// else ok = new user( email body & password is a hash)

// --- MASK FUNC to email-----
const maskEmail = email => {
  const mask = str => {
    if (str.length > 4) {
      return (
        str.substr(0, 1) +
        str.substr(1, str.length - 1).replace(/\w/g, '*') +
        str.substr(-1, 1)
      )
    }
    return str.replace(/\w/g, '*')
  }
  return email.replace(/([\w.]+)@([\w.]+)(\.[\w.]+)/g, function (
    m,
    p1,
    p2,
    p3
  ) {
    return mask(p1) + '@' + mask(p2) + p3
  })
  return email
}
// SIGNUP
exports.signup = (req, res) => {
  if (validator.isEmail(req.body.email) === false) {
    res.status(400).json({ message: 'L\'email n\'est pas au bon format' })
    return
  }
  if (validator.isStrongPassword(req.body.password) === false) {
    res.status(400).json({ message: 'Le mot de passe n\'est pas assez fort: 8 lettres dont une mminuscule, une majuscule, un chiffre et un symbole' })
    return
  }
  bcrypt
    .hash(req.body.password, 10)
    .then((hash) => {
      const user = new User({
        email: maskEmail(req.body.email),
        password: hash
      })
      user
        .save()
        .then(() => res.status(201).json({ message: 'Votre compte est créé!' }))
        .catch(error => res.status(400).json({ error }))
    })
}

// connect/login
// if email( not correct format ) & if password (not strong) = separate condition but necessary both are ok
// if correct = find email User => ok or not found
// if ok compare passwords (logged & typped)
// if ok => give token ( see doc npm JWT)

exports.login = (req, res) => {
  if (validator.isEmail(req.body.email) === false) {
    res.status(400).json({ message: 'L\'email n\'est pas au bon format' })
    return
  }
  if (validator.isStrongPassword(req.body.password) === false) {
    res.status(400).json({ message: 'Votre mot de passe doit contenir un minimum de 8 caractères, une minuscule, une majuscule, un symbole et un chiffre' })
    return
  }
  User.findOne({ email: maskEmail(req.body.email) })
    .then(user => {
      if (!user) {
        return res.status(400).json({ error: 'Compte non trouvé' })
      }
      bcrypt.compare(req.body.password, user.password)
        .then(valid => {
          if (!valid) {
            return res.status(401).json({ error: 'Votre mot de passe est incorrect' })
          }
          res.status(201).json({
            // token
            userId: user._id,
            token: jsonWebToken.sign({ userId: user._id },
              'RANDOM_TOKEN_SECRET'),
            expiresIn: '12h'
            // message:`Vous êtes connecté(e)`
          })
        })
        .catch(error => res.status(500).json({ error }))
    })
    .catch(error => res.status(500).json({ error }))
}
