# ![logo](./images/logo.jpg)
***
## Présentation de l'application
### l'Entreprise 
So Pekocko est une entreprise familiale de 10 salariés. Son activité principale est la création de sauces piquantes dont la composition est tenue secrète. Forte de son succès, l’entreprise souhaite se développer et créer une application web, dans laquelle les utilisateurs pourront ajouter leurs sauces préférées et liker ou disliker les sauces proposées par les autres.
###  __Une Architecture MVC__ ♺
#### La partie Front-end 💻
La partie View(v) a été développée à part. Nous reviendrons sur ses caractéristiques dans la partie __Installation__ 🛠
#### La partie Back-end
##### *L'API*
Elle permettra de faire le lien avec la Base de données(MongoDB)

**Les fonctionnalités de l'application** :
- S'inscrire et/ se connecter (signup et login)
- consulter les sauces ajoutées par les utilisateurs
- Ajouter des sauces
- Modifier ou supprimer des sauces (uniquement celles créées par l'utilisateur)
- Liker ou disliker les sauces

**La sécurité** :
- l’API doit respecter le RGPD et les standards OWASP
- le mot de passe des utilisateurs doit être chiffré
- 2 types de droits administrateur à la base de données doivent être définis:
    - un accès pour supprimer ou modifier des tables
    - et un accès pour éditer le contenu de la base de données 
- la sécurité de la base de données MongoDB (à partir d’un service tel que MongoDB Atlas) doit être faite de telle sorte que le validateur puisse lancer l’application depuis sa machine
- l’authentification est renforcée sur les routes requises
- les mots de passe sont stockés de manière sécurisée ;
- les adresses mails de la base de données sont uniques et un plugin Mongoose approprié est utilisé pour s’assurer de leur caractère unique et rapporter des erreurs.


### **Installation** 🛠

*Partie Frontend* :
Dans le terminal :
1.  Cloner (```git clone```) depuis ce repositrory [Github](https://github.com/OpenClassrooms-Student-Center/dwj-projet6)

2. Accéder au dossier `frontend`, puis
    ```
    npm install
    ng serve
    ```
3. Dans le navigateur, se rendre à l'adresse [Pekocko](http://localhost:4200)


**Partie Backend :**
Cloner depuis ce lien [Gitlab](https://gitlab.com/Renald_Bezet_Hubert/sopekocko.git)

Il faut ensuite créer un fichier `.env` à la racine du répertoire `backend` et copier le texte présent dans le fichier `env.` communiqué. Enregistrer le fichier.

Depuis le terminal, accéder au dossier `backend`, puis
```
npm install
nodemon server
```

Vous devriez pouvoir vous connecter à MongoDB et utiliser l'application pour pouvoir la tester.


### A Savoir

**Versions utilisées :** 
- node.js v14.16.1
- Angular ( version: Locale | Globale  ) 7.0.2 | 11.2.6


**Page d'inscription :**
Par mesure de sécurité, les champs requièrent une adresse email valide et un mot de passe sécurisé.
La complexité du mot de passe nécessite une majuscule, une minuscule, de 7 à 15 caractères et au moins un caractère spécial suivants : ! @ # $ % ^ & *.